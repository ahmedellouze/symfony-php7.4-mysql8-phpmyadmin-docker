# Symfony

première projet sous symfony 5.4.9.

##Environnement de développement

### Pré-requi
*PHP 7.4.27

*nginx  	1.20.2

*Mysql  	8.0

*PhpMyAdmin  	5.1.1

*Node-Js   	17.4.0

*Yarn  	1.22.17

Vous pouvez vérifier les pré-requis avec la commande suivante de la CLI symfony

bash

symfony chak:resuirements 
##Commande utils
###Creation d'un nouvelle Entitée
symfony console make:entity

###Preparation fichier de migration 
symfony console make:migration
###Remanté les donées dan la BDD
symfony console d:m:m
### Création des Entitées de Test
symfony console make:unit-test

### Lancer des tests
php bin/phpunit --testdox


 
